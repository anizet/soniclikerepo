﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadOnClick : MonoBehaviour
{
    public GameObject loadingImage;

    public void MyLoadScene(int levelIndex)
    {

        loadingImage.SetActive(true);

        // levelIndex is the scene index in build params
        SceneManager.LoadScene(levelIndex);   
    }
}
