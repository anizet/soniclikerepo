﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public List<Sprite> spriteList;

    public GameObject startButton;
    public GameObject victoryText;
    public GameObject deathText;

    public bool isDead = false;

    public float force;

    private SpriteRenderer playerSR;
    private Rigidbody2D playerRB;

    private int playerState = 0;

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("game started");
        isDead = false;
        // Get player sprite renderer to change color when pressing keys
        playerSR = GetComponent<SpriteRenderer>();

        // Get player rigid body to apply forces and then get movement
        playerRB = GetComponent<Rigidbody2D>();
    }

    public void StartBall()
    {
        //Debug.Log("Start ball");
        playerRB = GetComponent<Rigidbody2D>();
        playerRB.simulated = true;
        //playerRB.AddForce(Vector2.right * force * 500);
        startButton.SetActive(false);
    }

    public void SetPlayerColor(int colorIndex)
    {
        playerSR.sprite = spriteList[colorIndex];
        playerState = colorIndex;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        // Debug.Log("Collision Detected !");

        if (other.gameObject.CompareTag("JumpPad") && playerState == 1)
        {
            playerRB.AddForce(Vector2.up * force * 200);
            //Debug.Log("JumpPad colision");
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Trigger !!!");

        if (other.gameObject.CompareTag("Death"))
        {
            //Debug.Log("GAME OVER");
            isDead = true;
            deathText.SetActive(true);
            Invoke("RestartLevel", 1);
        }


        if (other.gameObject.CompareTag("Finish"))
        {
            //Debug.Log("VICTORYYYYYYY");
            victoryText.SetActive(true);
            Invoke("GoBackToMenu", 2);
        }
    }

    public void GoBackToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
